/* 1=Gestion du panier dans la page globale du panier */
/*variables js gestion du panier   */
let minus = $("input.minus")
/* ---affichage du nombre d'éléménts dans le panier-------*/
//let cart_nbr=$('span.cart_nbr')
//cart_nbr.text(minus.length)
/*---------------*/
let plus = $("input.plus")
let quantite = $("span.article-minus")
let url = $("form.myForm")
let qtePrix = $('td.qtePrix')
let prixArticle = $('td.prixArticle')

for (let i = 0; i < minus.length; i++) {
    minus[i].addEventListener('click', function (e) {
        e.preventDefault()
        console.log(i)
        console.log(quantite[i])
        if (quantite[i].textContent > 1) {
            let nb = quantite[i].textContent = Number(quantite[i].textContent) - 1;
            let prix = prixArticle[i].textContent
            let prix_quantite = prix * nb
            qtePrix[i].textContent = prix_quantite.toFixed(2);
            axios({
                method: 'post',
                url: url[i].action,
                params: {
                    nb: nb
                }
            })
                .then(function (reponse) {
                    //On traite la suite une fois la réponse obtenue
                    console.log(reponse.data.total_panier);
                    //  let resultat = number_format(prix_quantite, 2, ',')
                    let resultat=reponse.data.total_panier
                    $('#total').text(resultat.toFixed(2))
                })
                .catch(function (erreur) {
                    //On traite ici les erreurs éventuellement survenues
                    console.log('bad')
                    console.log(erreur);
                });
        }
    });
}

for (let i = 0; i < plus.length; i++) {
    plus[i].addEventListener('click', function (e) {
        e.preventDefault()
        if (quantite[i].textContent < 100) {
            let nb = quantite[i].textContent = Number(quantite[i].textContent) + 1;
            let prix = prixArticle[i].textContent
            let prix_quantite = prix * nb;

            qtePrix[i].textContent = prix_quantite.toFixed(2);
            axios({
                method: 'post',
                url: url[i].action,
                params: {
                    nb: nb
                }
            })
                .then(function (reponse) {
                    //On traite la suite une fois la réponse obtenue
                    // let resultat = number_format(prix_quantite, 2, ',')
                    let resultat=reponse.data.total_panier

                    $('#total').text(resultat.toFixed(2))
                })
                .catch(function (erreur) {
                    //On traite ici les erreurs éventuellement survenues
                    console.log('bad')
                    console.log(erreur);
                });
        }
    })
}
