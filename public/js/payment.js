window.onload = () => {
    // Variables
    //stripe ici c'est la clé public et locale : fr / sinon rien = auto STripe d'adaptera automatiquement à langue locale déttectée
    let stripe = Stripe('pk_test_51HOu0SIb9ZuOHpr2RG1PSlH4McREbDgoa4LFVWoAsyu1Hb8fh8iYobIlHYEKOlkAAFKnexCJIY7jvtCC3YlzzIud00olE6prR5',{
        locale: 'fr'
    })
    let elements = stripe.elements()
    console.log(elements)
    let redirect = document.getElementById('url').value
    //test du redirect
    console.log(redirect)

    // Objets de la page
    //Nom de la personne qui fait le paiment
    let cardHolderName = document.getElementById("cardholder-name")
    let cardButton = document.getElementById("card-button")
    //la clé qui nous vient d'intent et qui fait le lien avec la clé public
    let clientSecret = cardButton.dataset.secret;

    // Crée les éléments du formulaire de la carte bancaire
    let card = elements.create("card")
    console.log('l objet carte');
    //les monter sur l'id card-elements
    card.mount("#card-elements")

    // On gère la saisie, à la modification d'une valeur (change )
    //Vérification si la carte "fonctionne" (numéros et cles valides )
    card.addEventListener("change", (event) => {
        let displayError = document.getElementById("card-errors")
        if(event.error){
            displayError.textContent = event.error.message;
        }else{
            displayError.textContent = "";
        }
    })
    // On gère le paiement , gestion avec les promesses
    cardButton.addEventListener("click", () => {
        //lancement de la promesse =>on demande à stripe de gérer le paiemnt en fait
        stripe.handleCardPayment(
            //on envoie notre code secret , et les informations de la carte
            clientSecret, card, {
                payment_method_data: {
                    billing_details: {name: cardHolderName.value}
                }
            }
        ).then((result) => {
            if(result.error){
                document.getElementById("errors").innerText = result.error.message
            }else{
               alert('Merci '+cardHolderName.value+" ,votre paiment a bien été pris en compte")
                document.location.href = redirect;
            }
        })
    })
}
