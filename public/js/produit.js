
$('#add-ingredient').click(function(){
    //const index= $('#produit_ingredients div.form-group').length;  employée au départ mais provoque un bug sur l'index des éléments#}
    //const index=$('#produit_ingredients div.form-group').length;#}
    //Meilleure approche de la gestion de l'index du sous formulaire le + pour retenir le resultat en int et non pas en string, je définis un conteur#}
    const index=+$('#widgets-counter').val();


    //je récup le prototype des entrées  #}
    // et on introduit une expression regulière qui va  remplacer par index le name , le drapeau g-> pour dire qu'il faut le faire plusiseurs fois#}
    const tmpl=$('#produit_ingredients').data('prototype').replace(/__name__/g,index)


    //j'injecte ce code dans la div produit_ingredients  le prototype modifié de l'index#}
    //ce qui va ajouter les champs label,check ,le bouton delete directement#}
    $('#produit_ingredients').append(tmpl);

    //J'incrémente le conteur :
    $('#widgets-counter').val(index+1);


    //j'appelle deja la fonction de gestion des delete ici #}
    handleDeleteButtons();
    updatecounter();
});

/*Il est question ici de gere en js la suppression de champs d'une image introduite */
function handleDeleteButtons(){
    $('button[data-action="delete"]').click(function(){
        const target=this.dataset.target
        console.log(target);
        $(target).remove();
    });
}

function updatecounter(){
    const count=+$('#produit_ingredients div.form-group').length;
    $('#widget-counter').val(count);
}
// j'appelle cette fonction de gestion des delete ici au chargement de la page #}
    handleDeleteButtons();
    updatecounter();



