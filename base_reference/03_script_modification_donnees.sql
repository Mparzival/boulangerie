-- Mick le 13/08/20 à 16H45 --
-- Insertion dans la table produit --
ALTER TABLE produit ADD is_jour TINYINT(1) NOT NULL, ADD is_promo TINYINT(1) NOT NULL, ADD is_actif TINYINT(1) NOT NULL;