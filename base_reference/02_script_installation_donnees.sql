-- Remise à zero de la table type --
TRUNCATE TABLE `type`;
-- Insertion dans la table type --
INSERT INTO `type`(`id`, `label`) VALUES (1,'Pain'),
(2,'Salade'),
(3,'Sandwich'),
(4,'Patisserie'),
(5,'Viennoiserie'),
(6,'Boisson');