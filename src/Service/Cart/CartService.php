<?php

namespace App\Service\Cart;

use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class CartService{

    protected $session;
    protected $produitRepository;
    protected $request;

    public function __construct(SessionInterface $session,ProduitRepository $produitRepository,RequestStack $requestStack)
    {
        $this->session=$session;
        $this->produitRepository=$produitRepository;
        $this->requestStack=$requestStack;


    }

    /**
     * @param int $id
     * Function add
     * User: emayemba
     * Date: 25/08/2020
     */
    public function add(int $id)
    {
        $panier=$this->session->get('panier',[]);
        $request=$this->requestStack->getCurrentRequest();

        $nb=$request->get('nb');

        if(!empty ($nb) && $nb<100 ){
            //intval fonction native : retourne la valeur entière d'une variable
            $panier[$id]=intval($nb);
        }else{
            //$panier[$id]=1;
        }

        $this->session->set('panier',$panier);
    }

    /**
     * @param int $id
     * Function remove
     * User: emayemba
     * Date: 25/08/2020
     */
    public function remove(int $id)
    {
        $panier=$this->session->get('panier',[]) ;
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        $this->session->set('panier',$panier);
    }

    public function getFullCart():array
    {
        $panier= $this->session->get('panier',[]);
        $panierWithdata=[];
       foreach ($panier as $id=>$quantity){
            $panierWithdata[]=[
                'produit'=>$this->produitRepository->find($id),
                'quantity'=>$quantity];
        }
        return $panierWithdata;
    }

    public function getTotal():float
    {
        $total=0;
        foreach ($this->getFullCart() as $item){
            $total+=$item['produit']->getPrix() * $item['quantity'];
        }
        return $total;
    }

    public function endPaymentSucces(SessionInterface $session)
    {
        return $session->set('panier',[]);
    }
/*
    /**
     * @param int $id
     * Function remove
     * User: emayemba
     * Date: 25/08/2020
     */
  /*  public function oneQuantity(int $id)
    {
        $panier=$this->session->get('panier',[]) ;
        return count($panier[$id]);
    }*/


}
