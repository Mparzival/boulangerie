<?php

namespace App\Form;

use App\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

class IngredientType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label',TextType::class,[
                'label' => 'Nom',
                'label_attr' => ['class' => 'col-sm-3 col-form-label'],
                'attr'=>[
                    'placeholder'=>'Nom de l\'ingrédient ',
                    'class' => 'form-control'
                ]
            ])
            ->add('isAllergen',ChoiceType::class,[
                'required'=>true,
                'label' => 'Allergène',
                'label_attr' => ['class' => 'col-sm-3 col-form-label' ],
                'attr' => [],
                'expanded' => true,
                'choices' => [
                    $this->translator->trans('Yes') => 1,
                    $this->translator->trans('No') => 0,
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
