<?php

namespace App\Form;
use App\Entity\Ingredient;
use App\Entity\Produit;
use App\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

class ProduitType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label',TextType::class)
            ->add('imageFile',FileType::class,[
                'required'=> false])
            ->add('prix',MoneyType::class, [
                'currency' => 'EUR',
            ])
            ->add('type',EntityType::class,[
                     'class'=>Type::class,
                      'choice_label'=>'label'
            ])
            ->add('ingredients',EntityType::class,[
                'class'=>Ingredient::class,
                'multiple' => true,
                'choice_label' => 'label',
                'required' => false
            ])
            ->add('isActif', ChoiceType::class,[
               'choices' => [
                   $this->translator->trans('Yes') => 1,
                   $this->translator->trans('No') => 0,
                ],
                'required' => true,
                'expanded' => true,
            ])
            ->add('isJour', ChoiceType::class,[
                'choices' => [
                    $this->translator->trans('Yes') => 1,
                    $this->translator->trans('No') => 0,
                ],
                'required' => true,
                'expanded' => true,
            ])
            ->add('isPromo', ChoiceType::class,[
                'choices' => [
                    $this->translator->trans('Yes') => 1,
                    $this->translator->trans('No') => 0,
                ],
                'required' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
