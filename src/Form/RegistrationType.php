<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',TextType::class,[
                'label'=>'Votre Prénom'
            ])
            ->add('lastName',TextType::class,[
                'label'=>'Votre Nom'
            ])
            ->add('email',EmailType::class)
            ->add('password',RepeatedType::class,[
                'type'=>PasswordType::class,
                'invalid_message'=>'Les mots de passe sont différents ',
                'options' => ['attr' => ['class' => 'password']],
                'required' => true,
                'first_options'  => ['label' => 'Mot de passe '],
                'second_options' => ['label' => 'Confirmation du mot de Passe '],
            ])
            ->add('roles', HiddenType::class, [
                'data' => 'ROLE_USER',
            ])
           /* ->add('createdAt',HiddenType::class,[
                'empty_data'=> (new \DateTime('now'))
            ])*/
            ->add('agreeTerms', CheckboxType::class, [
                'label'=>'Acceptation des CGV',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
