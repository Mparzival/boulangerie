<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Form\ResetPasswordType;
use App\Form\RetrievePassType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AccountController extends AbstractController
{

    /**
     * @Route("/account/registration",name="account_registration")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param MailerInterface $mailer
     * Function inscription
     * User: emayemba
     * Date: 29/08/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function inscription(Request $request, EntityManagerInterface $entityManager,
                                UserPasswordEncoderInterface $encoder, GuardAuthenticatorHandler $guardHandler, MailerInterface $mailer)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $passwordencode = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($passwordencode);
            //Je génére un token et je l'enregistre
            $user->setActivationToken(md5(uniqid()));
            $entityManager->persist($user);
            $entityManager->flush();
            $email = (new TemplatedEmail())
                // On attribue l'expéditeur
                ->from('ericmick6@gmail.com')
                // On attribue le destinataire
                ->to($user->getEmail())
                //priorité du mail
                ->priority(Email::PRIORITY_HIGHEST)
                //sujet du mail à définir
                ->subject('Activation de votre compte pour accéder à boulangerie.com')
                // On crée le texte avec la vue

                // envoie du mail
                ->htmlTemplate('emails/activation.html.twig')
                ->context([
                    'token' => $user->getActivationToken(),
                    'user' => $user
                ]);
            $mailer->send($email);
            //return $this->redirectToRoute('account_login');
            return $this->render('account/attente_activation.html.twig');
        }
        return $this->render('account/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $token
     * @param UserRepository $userRepository
     * @Route("/account/activation/{token}",name="account_activation")
     * Gestion du lien d'activation avec la prise en compte de son délai de traitement
     * Function activation
     * User: emayemba
     * Date: 29/08/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function activation($token, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        //1.On recherche si un utilisateur possède ce token dans la base
        $user = $userRepository->findOneBy(['activation_token' => $token]);
        $dateNow = new \DateTime('now');

        // 2.Si aucun utilisateur n'est associé à ce token
        if (!$user) {
            // On renvoie une erreur 404
            throw $this->createNotFoundException('Cet utilisateur   n\'existe pas dans notre base ou actf? (a améliorer ) ');
        }
        //3.Si le délai d'activation du lien est dépassé:difference entre la date now et la createdAt
        if (date_diff($dateNow, $user->getCreatedAt())->format('%i') > 2) {
            // $entityManager = $this->getDoctrine()->getManager();
            //On supprime le user créé en amont
            $entityManager->remove($user);
            $entityManager->flush();
            return $this->render('account/activation_depasse.html.twig');
        }
        // On modifie le token à actif
        $user->setActivationToken("actif");
        //$entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        // On génère un message
        $this->addFlash('success', 'Utilisateur activé avec succès');
        //return $this->redirectToRoute('home');
        return $this->render('account/confirmation_validation.html.twig');
    }

    /**
     * @param AuthenticationUtils $utils
     * Function login
     * @Route("account/login",name="account_login")
     * Fonction de login à l'application avec gestion des erreurs
     * User: emayemba
     * Date: 26/08/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $utils)
    {
        return $this->render('account/login.html.twig', [
            'error' => $utils->getLastAuthenticationError(),
            'lastUserName' => $utils->getLastUsername()
        ]);
    }

    /**
     * Function logout
     * User: emayemba
     * @Route("account/logout",name="account_logout")
     * Fonction de logout de l'application
     * Date: 26/08/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout()
    {
        return $this->redirectToRoute('home');
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @param TokenGeneratorInterface $tokenGenerator
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface $mailer
     * @Route("/account/forgot/retrieveMail",name="account_forgotPassword")
     * Fonction permettant la gestion de perte de mot par le controle de l'email et de l'envoi par mail d'un token pour
     * reset par la suite le mot de passe
     * Function forgotPassword
     * User: emayemba
     * Date: 31/08/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function forgotPassword(Request $request, UserRepository $userRepository,
                                   TokenGeneratorInterface $tokenGenerator, EntityManagerInterface $entityManager, MailerInterface $mailer)
    {
        $form = $this->createForm(RetrievePassType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();

            $user = $userRepository->findOneBy(['email' => $email]);
            if ($user === null) {
                $this->addFlash('success', 'Cette adresse e-mail est inconnue');
                // On retourne sur la page de connexion
                return $this->redirectToRoute('account_login');
            }
            $token = $tokenGenerator->generateToken();

            try {
                $user->setPasswordToken($token);
                $entityManager->persist($user);
                $entityManager->flush();

            } catch (\Exception $e) {
                return $this->redirectToRoute('account_login');
            }

            $emailSend = (new TemplatedEmail())
                // On attribue l'expéditeur
                ->from('ericmick6@gmail.com')
                // On attribue le destinataire
                ->to($email)
                //priorité du mail
                ->priority(Email::PRIORITY_HIGHEST)
                //sujet du mail à définir
                ->subject('A votre demande vous allez rénitiliser votre mot de passe ')
                // On crée le texte avec la vue
                // envoie du mail
                ->htmlTemplate('emails/retrieve_password.html.twig')
                ->context([
                    'token' => $token,
                    'user' => $user
                ]);
            $mailer->send($emailSend);
            //return $this->redirectToRoute('account_login');
            return $this->render('account/attente_activation_resetPassword.html.twig');
        }
        return $this->render('account/verif_email.html.twig', [
            'form' => $form->CreateView()
        ]);
    }

    /**
     * @param $token
     * @param UserRepository $userRepository
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @Route("/forgot/resetPassword/{token}",name="account_resetPassword")
     * Cette fonction permet  de reset le mot de passe du user grâce au token reçu de la fonction forgotPassword()
     * Le formulaire n'est pas LIE A USER , on passe par les forms de symfony qui gerent déjà une securité en plus
     * Function resetPassword
     * User: emayemba
     * Date: 31/08/2020
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPassword($token, UserRepository $userRepository, Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $user = $userRepository->findOneBy(['password_token' => $token]);
        if ($user === null) {
            $this->addFlash('danger', 'Token Inconnu');
            return $this->redirectToRoute('account_login');
        }
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPassword = $form->get('newPassword')->getData();
            $newPasswordEncode = $encoder->encodePassword($user, $newPassword);
            $user->setPassword($newPasswordEncode);
            $entityManager->persist($user);
            $entityManager->flush();
            // On crée le message flash
            $this->addFlash('message', 'Mot de passe mis à jour');
            // On redirige vers la page de connexion
            return $this->redirectToRoute('account_login');
        }
        return $this->render('account/resetPassword.html.twig', [
            'form' => $form->createView(),
        ]);

    }


}
