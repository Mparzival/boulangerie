<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class HomeController extends AbstractController
{

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator){
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="accueil")
     */
    public function accueil()
    {
        return $this->render('home/accueil.html.twig');
    }

    /**
     * @Route("/home", name="home")
     */
    public function index(ProduitRepository $produitRepository)
    {
        $produitsJour = $produitRepository->findByProductOfDay();
        $produitPromo = $produitRepository->findByProductInPromotion();
        return $this->render('home/index.html.twig', [
            'produits' => $produitsJour,
            'produitsPromo' => $produitPromo,
        ]);
    }

    /**
     * @Route("/change_locale/{locale}", name="change_locale")
     */
    public function changeLocale($locale, Request $request)
    {
        // on stocke la langue demandé dans la session
        $request->getSession()->set('_locale', $locale);
        // on revient sur la page précédente
        return $this->redirect($request->headers->get('referer'));
    }
}
