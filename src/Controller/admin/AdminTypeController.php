<?php

namespace App\Controller\admin;

use App\Entity\Ingredient;
use App\Entity\Type;
use App\Form\TypeType;
use App\Repository\TypeRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminTypeController extends AbstractController
{
    /**
     * @Route("/admin/type", name="admin_type")
     */
    public function index(TypeRepository $typeRepository)
    {
        return $this->render('admin/type/index.html.twig', [
            'types' => $typeRepository->findAll(),
        ]);
    }

    /**
     * @param Type $type
     * @param TypeRepository $typeRepository
     * Function indexSlug
     * @Route("/admin/type/menu/{label}", name="admin_type_menu")
     * User: emayemba
     * Date: 08/08/2020
     * @return Response
     */
    public function indexMenu(Type $type):Response
    {
        return $this->render('admin/admin_type/showType.html.twig', [
            'type' => $type,
        ]);
    }

    /**
     * Function create
     * @Route("/admin/type/create", name="admin_type_create")
     * User: emayemba
     * Date: 06/08/2020
     * @param Request $request
     * @param EntityManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(Request $request,EntityManagerInterface $manager)
    {
        $type=new Type();

        $form=$this->createForm(TypeType::class,$type);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $manager->persist($type);
            $manager->flush();
            $this->addFlash('success',"la création a été effectuée ");
            return $this->redirectToRoute('admin_type');
        }
        return $this->render('admin/type/creationType.html.twig',[
            'form'=>$form->createView(),
        ]);
    }

    /**
     * Function modification
     * @Route("/admin/type/update/{id}", name="admin_type_update")
     * User: emayemba
     * Date: 06/08/2020
     * @param Request $request
     * @param EntityManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Request $request,EntityManagerInterface $manager,Type $type):Response
    {
        $form=$this->createForm(TypeType::class,$type);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())

        {
            $manager->persist($type);
            $manager->flush();
            $this->addFlash('success',"la modification a été effectuée ");
            return $this->redirectToRoute('admin_type');
        }
        return $this->render('admin/type/modificationType.html.twig',[
            'form'=>$form->createView(),
            'type'=>$type
        ]);
    }

    /**
     * @param EntityManagerInterface $manager
     * @param Type $type
     * @Route("/admin/type/delete/{id}",name="admin_type_delete")
     * Function delete
     * User: emayemba
     * Date: 06/08/2020
     */
    public function delete(EntityManagerInterface $manager,Type $type)
    {
       $manager->remove($type);
       $manager->flush();
       $this->addFlash('success',"la suppression a été effectué ");
       return $this->redirectToRoute('admin_type');
    }

    /**
     * @param TypeRepository $typeRepository
     * @param Type $type
     * @Route("/admin/type/show/{id}",name="admin_type_show")
     * Function show
     * User: emayemba
     * Date: 08/08/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(TypeRepository $typeRepository,Type $type):Response
    {
        return $this->render('admin/type/showType.html.twig',[
            'type'=>$type,
        ]);
    }
}
