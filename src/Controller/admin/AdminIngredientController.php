<?php

namespace App\Controller\admin;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ingredient", name="admin.ingredient.")
 */
class AdminIngredientController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="index")
     * @param IngredientRepository $repository
     * @return Response
     */
    public function index(IngredientRepository $repository)
    {
        $ingredients = $repository->findAll();
        return $this->render(
            'admin/ingredient/liste.html.twig', [
            'ingredients' => $ingredients
        ]);
    }

    /**
     * @Route("/create", name="create", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request) : Response
    {
        $ingredient = new Ingredient();
        $form= $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($ingredient);
            $this->manager->flush();
            $this->addFlash('success',"la création a été effectuée ");
            return $this->redirectToRoute('admin.ingredient.index');
        }
        return $this->render('admin/ingredient/creationIngredient.html.twig',[
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param Ingredient $ingredient
     * @return Response
     */
    public function edit(Request $request, Ingredient $ingredient) : Response
    {
        $form= $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->persist($ingredient);
            $this->manager->flush();
            $this->addFlash('success',"la création a été effectuée ");
            return $this->redirectToRoute('admin.ingredient.index');
        }
        return $this->render('admin/ingredient/modificationIngredient.html.twig',[
            'form'=>$form->createView(),
            'ingredient' => $ingredient
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete", methods={"DELETE"})
     * @param Ingredient $ingredient
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Ingredient $ingredient, Request $request)
    {
        if ($this->isCsrfTokenValid('delete'.$ingredient->getId(), $request->request->get('_token'))) {
            $this->manager->remove($ingredient);
            $this->manager->flush();
        }

        return $this->redirectToRoute('admin.ingredient.index');
    }
}
