<?php

namespace App\Controller\admin;

use App\Entity\Ingredient;
use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/produit")
 */
class AdminProduitController extends AbstractController
{
    /**
     * @Route("/", name="admin_produit")
     * @param ProduitRepository $produitRepository
     * @return Response
     */
    public function index(ProduitRepository $produitRepository)
    {
        return $this->render('admin/produit/index.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    /**
     * Function create
     * @Route("/create", name="admin_produit_create")
     * User: emayemba
     * Date: 06/08/2020
     * @param Request $request *
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function create(Request $request,EntityManagerInterface $manager)
    {
        $produit=new Produit();

        /*Utilisé au début pour le prototype type
         * $ingredient=new Ingredient();
        $ingredient->setLabel('Test Ingredient 1308 ');
        $ingredient->setIsAllergen(true);

        $ingredient2=new Ingredient();
        $ingredient2->setLabel('Test Ingredient 1308_2 ');
        $ingredient2->setIsAllergen(true);

        $produit->addIngredient($ingredient);
        $produit->addIngredient($ingredient2);*/

        $form=$this->createForm(ProduitType::class,$produit);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            foreach ($produit->getIngredients() as $ingredient){
                $ingredient->addProduit($produit);
                $manager->persist($ingredient);
            }
            $manager->persist($produit);
            $manager->flush();
            $this->addFlash('success',"la création a été effectuée ");
            return $this->redirectToRoute('admin_produit');
        }
        return $this->render('admin/produit/creationProduit.html.twig',[
            'form'=>$form->createView(),
        ]);
    }

    /**
     * Function modification
     * @Route("/modifcation/{id}", name="admin_produit_modification")
     * User: emayemba
     * Date: 06/08/2020
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Produit $produit
     * @return Response
     */
    public function modification(Request $request,EntityManagerInterface $manager,Produit $produit)
    {
        $form=$this->createForm(ProduitType::class,$produit);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            foreach ($produit->getIngredients() as $ingredient){
                $ingredient->addProduit($produit);
                $manager->persist($ingredient);
            }

            $manager->persist($produit);
            $manager->flush();
            $this->addFlash('success',"la modification a été effectuée ");
            return $this->redirectToRoute('admin_produit');
        }
        return $this->render('admin/produit/modificationProduit.html.twig',[
            'form'=>$form->createView(),
            'produit'=>$produit
        ]);
    }

    /**
     * @param EntityManagerInterface $manager
     * @param Produit $produit
     * @Route("/delete/{id}",name="admin_delete_produit")
     * Function delete
     * User: emayemba
     * Date: 06/08/2020
     */
    public function delete(EntityManagerInterface $manager,Produit $produit)
    {
        $manager->remove($produit);
        $manager->flush();
        $this->addFlash('success',"la suppression a été effectuée ");
        return $this->redirectToRoute('admin_produit');
    }
}
