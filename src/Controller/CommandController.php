<?php

namespace App\Controller;

use App\Service\Cart\CartService;

use http\Client\Response;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;

use Stripe\StripeClientInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;




class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command_index")
     * @param SessionInterface $session
     * @param CartService $cartService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SessionInterface $session,CartService $cartService)
    {
        //$panier=$session->get('panier');
        $nCommandeProvisoireTest=(new \DateTime())->format('d/m/Y');;
        return $this->render('command/index.html.twig', [
            'panier' =>$cartService->getFullCart(),
            'total'=>$cartService->getTotal(),
            'nCommandeProvisoireTest'=>$nCommandeProvisoireTest

        ]);
    }

    /**
     * Function payment
     * @Route("/command/payment", name="command_payment")
     * User: emayemba
     * Date: 30/09/2020
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ApiErrorException
     */
    public function payment(Request $request)
    {
       $prix=floatval($request->request->get('prix'));

        // Nous instancions Stripe en indiquand la clé secrète (test) , pour prouver que nous sommes bien à l'origine de cette demande
        \Stripe\Stripe::setApiKey('sk_test_51HOu0SIb9ZuOHpr2cTII355hqKU5WDhyc8w8xfuPgcWmKt6pQG4ZS7Z0JY7tOIVoVDyva0wqBdNjANwYkXyeToka00OVosOW1B');

        //Nous créons l'intention de paiement et stockons la réponse dans la variable $intent
        $intent=\Stripe\PaymentIntent::create([
            'amount' => $prix*100,
            'currency' => 'eur',
            'payment_method_types' => ['card'],
            'receipt_email' => 'bolingo@yopmail.com',
        ]);
       /* return $this->json([
            'intent' => $intent,
        ],200);*/

        return $this->render('command/payment.html.twig',[
            'intent'=>$intent
        ]);
    }

    /**
     * Function endPayment
     * @Route("/command/success/end",name="cart_end_success")
     * User: emayemba
     * Date: 01/10/2020
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function endPayment(CartService $cartService,SessionInterface $session,MailerInterface $mailer)
    {
        $email=(new TemplatedEmail())
              ->from('ericmick6@gmail.com')
              ->to('bolingo@yopmail.com')
              ->priority(Email::PRIORITY_HIGHEST)
              ->subject('Commande ')
              ->htmlTemplate('emails/commande.html.twig')
              ->context([
                'panier'=>$cartService->getFullCart(),
                'total'=>$cartService->getTotal(),
            ]);
        $mailer->send($email);

       $cartService->endPaymentSucces($session);
        return $this->render('command/success_payment.html.twig');
    }


}
