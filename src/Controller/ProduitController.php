<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Enum\TypeEnum;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/produit")
 **/
class ProduitController extends AbstractController
{
    /**
     * @Route("/", name="produit")
     * @param ProduitRepository $produitRepository
     * @return Response
     */
    public function index(ProduitRepository $produitRepository)
    {
        $produits = $produitRepository->findAll();
        return $this->render(
            'produit/showProduitByType.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @param Produit $produit
     * @return Response
     * @Route("/show/{id}",name="produit_show")
     * Function show
     * User: emayemba
     * Date: 06/08/2020
     */
    public function show(Produit $produit){
        return $this->render(
            'produit/showProduit.html.twig', [
            'produit'=>$produit,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Pain
     * @Route("/pain", name="produit_pain")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexPain(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_PAIN);
        return $this->render(
            'produit/showProduitPain.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Salade
     * @Route("/salade", name="produit_salade")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexSalade(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_SALADE);
        return $this->render(
            'produit/showProduitSalade.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Sandwich
     * @Route("/sandwich", name="produit_sandwich")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexSandwich(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_SANDWICH);
        return $this->render(
            'produit/showProduitSandwich.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Viennoiserie
     * @Route("/viennoiserie", name="produit_viennoiserie")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexViennoiserie(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_VIENNOISERIE);
        return $this->render(
            'produit/showProduitViennoiserie.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Patisserie
     * @Route("/patisserie", name="produit_patisserie")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexPatisserie(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_PATISSERIE);
        return $this->render(
            'produit/showProduitPatisserie.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * fonction permettant d'accéder à une page contenant tous les produits
     * de type Boisson
     * @Route("/boisson", name="produit_boisson")
     * @param ProduitRepository $repository
     * @return Response
     */
    public function indexBoisson(ProduitRepository $repository)
    {
        $produits = $repository->findByType(TypeEnum::TYPE_BOISSON);
        return $this->render(
            'produit/showProduitBoisson.html.twig', [
            'produits' => $produits,
        ]);
    }
}
