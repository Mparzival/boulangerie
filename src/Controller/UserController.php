<?php

namespace App\Controller;

use App\Form\AccountType;
use App\Form\PasswordUpdateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserController extends AbstractController
{
    /**
     * @Route("/monCompte", name="user_monCompte")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
         'user'=>$user=$this->getUser()
        ]);
    }

    /**
     * Function userUpdate
     * User: emayemba
     * @Route("monCompte/update",name="user_update")
     * Date: 26/08/2020
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function userUpdate(Request $request,EntityManagerInterface $entityManager)
    {
        $user=$this->getUser();
        $form=$this->createForm(AccountType::class,$user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_monCompte');
        }

        return $this->render('user/userUpdate.html.twig',[
            'form'=>$form->createView()
        ]);

    }

    /**
     * Function updatePassword
     * @Route("monCompte/updatePassword",name="user_update_password")
     * User: emayemba
     * Date: 26/08/2020
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updatePassword(Request $request,EntityManagerInterface $entityManager,UserPasswordEncoderInterface $encoder){
        $user=$this->getUser();
        $form=$this->createForm(PasswordUpdateType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $oldPassword=$user->getPassword();
           $password=$form->get('oldpassword')->getData();

            if(password_verify($password,$oldPassword)){
              $newPassword=$form->get('newPassword')->getData();
              $newPasswordEncode=$encoder->encodePassword($user,$newPassword);
              $user->setPassword($newPasswordEncode);
              $entityManager->persist($user);
              $entityManager->flush();

              return $this->redirectToRoute('user_monCompte');

            }else{
                $form->get('oldpassword')->addError(new FormError('Votre mot de passe n\'est pas reconnu '));
            }

        }

        return $this->render('user/passwordUpdate.html.twig',[
            'form'=>$form->createView()
        ]);
    }
}
