<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Repository\ProduitRepository;
use App\Service\Cart\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/panier", name="cart_index")
     * @param CartService $cartService
     * @param SessionInterface $session
     * @return Response
     */
    public function index(CartService $cartService, SessionInterface $session)
    {
       if(empty($session->get('panier'))){
           $taillePanier=0;
       }$taillePanier=count($session->get('panier'));

       return $this->render('cart/index.html.twig', [
            'items' => $cartService->getFullCart(),
            'total' => $cartService->getTotal(),
            'panier' => $session->get('panier'),
           'taillePanier'=>$taillePanier

        ]);

    }

    /**
     * @param $id
     * @param CartService $cartService
     * @param Produit $produit
     * @return Response
     * @Route("/panier/add/{id}",name="cart_add")
     * Function add
     * User: emayemba
     * Date: 25/08/2020
     */
    public function add($id, CartService $cartService,Produit $produit)
    {

        $cartService->add($id);
        //return $this->redirectToRoute("cart_add");
        return $this->render('produit/showProduit.html.twig',[
            'produit'=>$produit
        ]);

    }

    /**
     * @param $id
     * @param CartService $cartService
     * @return RedirectResponse
     * @Route("/panier/remove/{id}", name="cart_remove")
     * Function remove
     * User: emayemba
     * Date: 25/08/2020
     */
    public function remove($id, CartService $cartService)
    {
        $cartService->remove($id);
        return $this->redirectToRoute("cart_index");
    }

    /**
     * @param $id
     * @param CartService $cartService
     * @return JsonResponse
     * @Route("/panier/add/api/{id}",name="cart_add_api")
     * Function add
     * User: emayemba
     * Date: 25/08/2020
     */
    public function addApi($id, CartService $cartService)
    {
        $cartService->add($id);
        return $this->json([
                'total_panier' => $cartService->getTotal(),
        ],200);
    }

}
