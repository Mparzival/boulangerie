<?php

namespace App\Enum;

class TypeEnum
{
    const TYPE_PAIN = 1;
    const TYPE_SALADE = 2;
    const TYPE_SANDWICH = 3;
    const TYPE_PATISSERIE = 4;
    const TYPE_VIENNOISERIE = 5;
    const TYPE_BOISSON = 6;
}