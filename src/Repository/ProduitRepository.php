<?php

namespace App\Repository;

use App\Entity\Produit;
use App\Enum\TypeEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    /**
     * Requête permettant de récupérer tous les produits du jour
     * @return int|mixed|string
     */
    public function findByProductOfDay()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.isJour = 1')
            ->orderBy('p.updatedAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * Requête permettant de récupérer tous les produits en promotion
     * @return int|mixed|string
     */
    public function findByProductInPromotion()
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.isPromo = 1')
            ->orderBy('p.updatedAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByType($typeProduit)
    {
        $q = $this->createQueryBuilder('p')
            ->select('p.id as id, p.label as label, p.prix as prix, p.imageProduit as imageProduit, ty.id as type')
            ->leftJoin('p.type', 'ty');

            if ($typeProduit == TypeEnum::TYPE_PAIN) {
              $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_PAIN);
            }
            if ($typeProduit == TypeEnum::TYPE_SALADE){
                $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_SALADE);
            }
            if ($typeProduit == TypeEnum::TYPE_SANDWICH){
                $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_SANDWICH);
            }
            if ($typeProduit == TypeEnum::TYPE_PATISSERIE){
                $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_PATISSERIE);
            }
            if ($typeProduit == TypeEnum::TYPE_VIENNOISERIE){
                $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_VIENNOISERIE);
            }
            if ($typeProduit == TypeEnum::TYPE_BOISSON){
                $q ->where('ty.id = :typeProduct')->setParameter('typeProduct', TypeEnum::TYPE_BOISSON);
            }

            return $q->orderBy('p.label', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
