<?php
namespace App\Security\User;

use App\Exception\DelayOutException;
use App\Entity\User as AppUser;
use Exception;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Cette class permet une verification complémentaire au systeme de login symfony
 */
class UserChecker implements UserCheckerInterface
{

    /**
     * @param UserInterface $user
     * Function checkPreAuth
     * User: emayemba
     * Date: 29/08/2020
     * @throws Exception
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if ($user->getActivationToken() !== 'active') {
            throw new Exception('Compte non actif merci de le valider par retour de mail ');
        }

         // user is deleted, show a generic Account Not Found message.
          //if ($user->isDeleted()) {
         //throw new Exception('Utilisateur effacé ');
         //}
    }

    /**
     * @param UserInterface $user
     * Function checkPostAuth
     * User: emayemba
     * Date: 29/08/2020
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

// user account is expired, the user may be notified
//if ($user->isExpired()) {
//throw new AccountExpiredException('...');
//}
    }
}
